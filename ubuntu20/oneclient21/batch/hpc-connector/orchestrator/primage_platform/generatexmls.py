from quiblib.writer import write_results
from quiblib.analysis_modules.base import AnalysisModule
from quiblib.config import Config
import quiblib as ql
import numpy as np
import argparse
import hashlib
from pathlib import Path
import os


class AnalysisError(Exception):
    """Base error related to White matter lesion analysis method"""


class AnalysisModuleExample(AnalysisModule):
    name = 'Example module'
    identifier = 'example_module'
    standard_series = ['t2w', 'adc', 'ktrans']
    rois_used = True
    specific_roi_labels = None


def get_folders_name(result_path, roi_label):
    hashed_label = hashlib.md5(roi_label.encode())
    roi_id = hashed_label.hexdigest()
    
    result_path_dir = os.path.dirname(result_path)

    os.mkdir(result_path_dir + "/Results")
    os.mkdir(result_path_dir + "/Report")

    results_roi = Path(result_path_dir) / 'Results' / roi_id
    report_roi = Path(result_path_dir) / 'Report' / roi_id

    os.mkdir(results_roi)
    os.mkdir(report_roi)

    return results_roi, report_roi


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Example module.')

    parser.add_argument("--respath", help="Path to results.txt file")

    args = parser.parse_args()
    
    path_analysis = Path("/analysis")
    path_results  = args.respath

    config = Config(AnalysisModuleExample)
    config.load_config(path_analysis)

    # We can get the mask to analyze from the series folder name
    path_adc = config.series['adc']['path']
    name = path_adc.name
    mask_name = name.split('ADC')[1]

    raw_rois = config.get_raw_rois()['t2w']
    roi_label = None
    for r in raw_rois:
        if r['label'] == mask_name:
            roi_label = r['label']
            break

    if roi_label is None:
        raise AnalysisError(f'ROI not found!')

    vols = np.genfromtxt(path_results)
    quantification = dict()
    quantification['Body'] = {
        'volumeIni': {'value': vols[0]},
        'volumeFin': {'value': vols[1]},
        'relativeReduction': {'value': vols[1] / vols[0]},
    }

    result_folder, report_folder = get_folders_name(path_results, roi_label)
    
    analysis = {'quantification': quantification,
                'roi_info': {'label': roi_label},
                'result_folder': result_folder,
                'report_folder': report_folder
                }

    analyses = [analysis]
    _, header = ql.read_series(config.series['t2w']['path'])


    write_results(config, analyses, header)
