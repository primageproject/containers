#!/usr/bin/env bash

if [ -n "$DISABLE_SMB" ]; then
    echo SMB disabled.
else
    # samba
    mkdir -p /run/samba
    smbd
    nmbd

    # nfs
    rpcbind
    rpc.nfsd
    exportfs -ar
    rpc.mountd
    rpc.nfsd
    rpc.statd
fi

echo -n "####################################### Mounting PRIMAGE_HOME and PRIMAGE_DATALAKE ftom EGI Datahub (and symbolic links) ####################################### " && date -Iseconds
if [ -n "$ONECLIENT_ACCESS_TOKEN" ]; then
    # oneclient -t $ONECLIENT_ACCESS_TOKEN -H $ONECLIENT_PROVIDER_HOST $MOUNT_POINT
    
    echo "oneclient -t $ONECLIENT_ACCESS_TOKEN -H $ONECLIENT_PROVIDER_HOST --space PRIMAGE_DATALAKE -o 'ro,allow_other' $MOUNT_POINT" > /root/mountDatalake.sh
    chmod o-rwx /root/mountDatalake.sh
    source /root/mountDatalake.sh
    
    if [ -z "$PERSISTENT_HOME_MOUNT_POINT" ]; then export PERSISTENT_HOME_MOUNT_POINT=$MOUNT_POINT-home; fi
    mkdir $PERSISTENT_HOME_MOUNT_POINT
    if [ -z "$ONECLIENT_PROVIDER_HOST_FOR_HOME" ]; then export ONECLIENT_PROVIDER_HOST_FOR_HOME=$ONECLIENT_PROVIDER_HOST; fi
    echo "oneclient -t $ONECLIENT_ACCESS_TOKEN -H $ONECLIENT_PROVIDER_HOST_FOR_HOME --space PRIMAGE_HOME -o 'rw,allow_other' $PERSISTENT_HOME_MOUNT_POINT" > /root/mountPersistentHome.sh
    chmod o-rwx /root/mountPersistentHome.sh
    source /root/mountPersistentHome.sh
    
    echo "oneclient -u $MOUNT_POINT" > /root/umountDatalake.sh
    echo "oneclient -u $PERSISTENT_HOME_MOUNT_POINT" > /root/umountPersistentHome.sh
    
    ln -s $PERSISTENT_HOME_MOUNT_POINT/PRIMAGE_HOME/$HOME_DIR_NAME /persistent-home
    
    if [ -n "$DATASET_REFERENCES_FILE" ]; then
        echo "======================="
        echo "Downloading last version of eform-downloader"
        curl -o /root/downloadEForms.py https://gitlab.com/primageproject/eform-downloader/-/raw/main/downloadEForms.py
        echo "Downloading Subject Eforms from $QUIBIM_HOST"
        python3 /root/downloadEForms.py --input $PERSISTENT_HOME_MOUNT_POINT/PRIMAGE_HOME/$HOME_DIR_NAME/$DATASET_REFERENCES_FILE --qp-host $QUIBIM_HOST \
                                        --user $QUIBIM_USER --password $QUIBIM_PASSWORD \
                                        --output-dir /dataset --qp-working-dir $MOUNT_POINT/PRIMAGE_DATALAKE
    fi
else
	echo "OneData access token not defined."
	exit 1
fi

echo -n "####################################### Crating a PlGrid proxy file ####################################### " && date -Iseconds
#The next lines create a proxy file required for submiting to clusters hosted at plGrid such as ares or prometheus
if [ -n "$PLGRID_USER" ]; then
	echo $PLGRID_PASSWORD | sshpass -p $PLGRID_PASSWORD ssh -o "StrictHostKeyChecking no" $PLGRID_USER@pro.cyfronet.pl "grid-proxy-init -q -pwstdin && cat /tmp/x509up_u\`id -u\`" > grid_proxy
	export GRID_PROXY="`cat grid_proxy | base64 | tr -d '\n'`"
else
	echo "PLGRID_USER not defined."
	exit 2
fi

echo -n "####################################### Creating Config files for HPC-Connector (HPCCONNECTOR config file and JOB_STRING ####################################### " && date -Iseconds
if [ -n "$GRID_PROXY" ]; then
	export HPCCONNECTOR='{"ENDPOINT":"https://submit.plgrid.pl","PROXY":"'$GRID_PROXY'"}'
	echo "HPCCONNECTOR config file created succesfully."
else
	echo "HPCCONNECTOR config file not defined because proxy is not created."
	exit 3
fi

#The next lines are for creating the JOB CONFIG for the HPC-CONNECTOR
JOB_SLURM_FILE=/root/job_slurm_template_orchestrator
JOB_STRING=$(cat $JOB_SLURM_FILE | sed ':a;N;$!ba;s/\n/\\n/g')
if [ -n "$INPUT_FOLDER" ]; then
	JOB_STRING="${JOB_STRING/'$INPUT_FOLDER'/$INPUT_FOLDER}"
	echo "In the JOB SLURM has been defined INPUT_FOLDER path succesfully"
else
	echo "In the JOB SLURM it is necessary to define INPUT_FOLDER variable. So, the JOB CONFIG would need to be reconfigured in the future with this variable before submiting the process to the HPC backend."
fi
if [ -n "$OUTPUT_FOLDER" ]; then
	JOB_STRING="${JOB_STRING/'$OUTPUT_FOLDER'/$OUTPUT_FOLDER}"
	echo "In the JOB SLURM has been defined OUTPUT_FOLDER path succesfully"
else
	echo "In the JOB SLURM it is necessary to define OUTPUTS_FOLDER variable. So, the JOB CONFIG would need to be reconfigured in the future with this variable before submiting the process to the HPC backend."
fi
if [ -n "$ONECLIENT_ACCESS_TOKEN" ]; then
	JOB_STRING="${JOB_STRING/'$ONECLIENT_ACCESS_TOKEN'/$ONECLIENT_ACCESS_TOKEN}"
else
	echo "In the JOB SLURM it is necessary to define ONECLIENT_ACCESS_TOKEN variable. So, the JOB CONFIG would need to be reconfigured in the future with this variable before submiting the process to the HPC backend."
fi

if [ -n "$ONECLIENT_PROVIDER_HOST" ]; then
	JOB_STRING="${JOB_STRING/'$ONECLIENT_PROVIDER_HOST'/$ONECLIENT_PROVIDER_HOST}"
else
	echo "In the JOB SLURM it is necessary to define ONECLIENT_PROVIDER_HOST variable. So, the JOB CONFIG would need to be reconfigured in the future with this variable before submiting the process to the HPC backend."
fi

if [ -n "$GRID_PROXY" ]; then
	JOB_STRING="${JOB_STRING/'$PROXY'/$GRID_PROXY}"
else
	echo "In the JOB SLURM it is necessary to define PROXY variable. So, the JOB CONFIG would need to be reconfigured in the future with this variable before submiting the process to the HPC backend."
fi

if [ -n "$DB_PASSWORD" ]; then
	JOB_STRING="${JOB_STRING/'$DB_PASSWORD'/$DB_PASSWORD}"
else
	echo "In the JOB SLURM it is necessary to define DB_PASSWORD (mySQL Database in the cluster) variable. So, the JOB CONFIG would need to be reconfigured in the future with this variable before submiting the process to the HPC backend."
fi
if [ -n "$ANALYSIS_PATH_FILE" ]; then
	JOB_STRING="${JOB_STRING/'$ANALYSIS_PATH_FILE'/$ANALYSIS_PATH_FILE}"
else
	echo "In the JOB SLURM it is necessary to define ANALYSIS_PATH_FILE (path of the analysis.json) variable. So, the JOB CONFIG would need to be reconfigured in the future with this variable before submiting the process to the HPC backend."
fi

export JOB_CONFIG='{"script":"'$JOB_STRING'","host":"'$HOST_BACKEND'"}'

echo "$@" >.runcmd
source .runcmd
