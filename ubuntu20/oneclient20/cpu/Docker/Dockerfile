FROM ubuntu:20.04

LABEL description "Container with Oneclient 20.02.18 and ubuntu 20.04 (focal) distribution."
MAINTAINER  J. Damian Segrelles Quilis (dquilis@dsic.upv.es)

# Build arguments for Oneclient
# We use a fixed version to avoid conflict with the provider (we must update it manually when we update the provider)
ARG RELEASE="2002"
ARG VERSION="20.02.18"
ARG DISTRIBUTION="focal"
ARG FSONEDATAFS_VERSION="20.02.18"
ARG ONECLIENT_PACKAGE="oneclient"
ARG INSTALL_URL="http://packages.onedata.org"

# Get the image up to date and install utility tools
RUN apt-get -y update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install ssh sshpass bash-completion ca-certificates curl iputils-ping netcat \
                       man-db net-tools traceroute vim parallel \
                       python3-pip gnupg2 xattr pv bsdmainutils && \
    		       apt-get clean &&\
    		       pip3 install --upgrade pip       # The ubuntu package is not always up-to-date

# Install fs.onedatafs dependencies
RUN pip install fs six setuptools xmlrunner pyyaml
RUN pip3 install fs six setuptools xmlrunner pyyaml

WORKDIR /tmp

# Temporarly disable dpkg path-exludes so that all Oneclient files are installed
# including docs and manpages
RUN mv /etc/dpkg/dpkg.cfg.d/excludes /tmp/excludes.bak

RUN curl -O ${INSTALL_URL}/oneclient-${RELEASE}.sh && \
	sh oneclient-${RELEASE}.sh ${ONECLIENT_PACKAGE}=${VERSION}-1~${DISTRIBUTION}

RUN pip install fs-onedatafs

RUN mv /tmp/excludes.bak /etc/dpkg/dpkg.cfg.d/excludes

# Enable autocompletion
RUN echo "source /etc/bash_completion" >> /root/.bashrc

# Install Samba and NFS servers
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install samba rpcbind nfs-kernel-server && \
    apt-get clean

# Add Samba and NFS configs
ADD nfs_exports /etc/exports
ADD smb.conf /etc/samba/

# Install poetry: popular dependecies manager, and used in the Quibim platform to install dependencies required for the image analysis modules.
# And disable virtual environments (in a container usually there is only one project, one environment)
RUN pip install poetry && poetry config virtualenvs.create false

# Add entrypoint script
ADD run.sh /root/run.sh
RUN chmod +x /root/run.sh

VOLUME /root/.local/share/oneclient /mnt/oneclient

ENV ONECLIENT_INSECURE=1

ENTRYPOINT ["/root/run.sh"]
