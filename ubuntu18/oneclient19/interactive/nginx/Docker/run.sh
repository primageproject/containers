#!/usr/bin/env bash

# samba
mkdir -p /run/samba
smbd
nmbd

# nfs
rpcbind
rpc.nfsd
exportfs -ar
rpc.mountd
rpc.nfsd
rpc.statd

sed 's@PRIMAGE_NGINX_DIRECTORY@'"$MOUNT_POINT\/$NGINX_DIR"'@' /root/default > /etc/nginx/sites-available/default 

oneclient -t $ONECLIENT_ACCESS_TOKEN -H $ONECLIENT_PROVIDER_HOST $MOUNT_POINT 

echo "$@" >.runcmd
source .runcmd
