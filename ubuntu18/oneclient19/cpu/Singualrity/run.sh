#!/bin/sh 

# samba
mkdir -p /run/samba
smbd
nmbd

# nfs
rpcbind
rpc.nfsd
exportfs -ar
rpc.mountd
rpc.nfsd
rpc.statd

oneclient -t $ONECLIENT_ACCESS_TOKEN -H $ONECLIENT_PROVIDER_HOST $MOUNT_POINT 

echo "$@" >.runcmd
sh .runcmd
