#!/usr/bin/env bash

if [ -n "$DISABLE_SMB" ]; then
    echo SMB disabled.
else
    # samba
    mkdir -p /run/samba
    smbd
    nmbd

    # nfs
    rpcbind
    rpc.nfsd
    exportfs -ar
    rpc.mountd
    rpc.nfsd
    rpc.statd
fi

if [ -n "$ONECLIENT_ACCESS_TOKEN" ]; then
    # oneclient -t $ONECLIENT_ACCESS_TOKEN -H $ONECLIENT_PROVIDER_HOST $MOUNT_POINT
    
    echo "oneclient -t $ONECLIENT_ACCESS_TOKEN -H $ONECLIENT_PROVIDER_HOST --space PRIMAGE_DATALAKE -o 'ro,allow_other' $MOUNT_POINT" > /root/mountDatalake.sh
    chmod o-rwx /root/mountDatalake.sh
    source /root/mountDatalake.sh
    
    if [ -z "$PERSISTENT_HOME_MOUNT_POINT" ]; then export PERSISTENT_HOME_MOUNT_POINT=$MOUNT_POINT-home; fi
    mkdir $PERSISTENT_HOME_MOUNT_POINT
    if [ -z "$ONECLIENT_PROVIDER_HOST_FOR_HOME" ]; then export ONECLIENT_PROVIDER_HOST_FOR_HOME=$ONECLIENT_PROVIDER_HOST; fi
    echo "oneclient -t $ONECLIENT_ACCESS_TOKEN -H $ONECLIENT_PROVIDER_HOST_FOR_HOME --space PRIMAGE_HOME -o 'rw,allow_other' $PERSISTENT_HOME_MOUNT_POINT" > /root/mountPersistentHome.sh
    chmod o-rwx /root/mountPersistentHome.sh
    source /root/mountPersistentHome.sh
    
    echo "oneclient -u $MOUNT_POINT" > /root/umountDatalake.sh
    echo "oneclient -u $PERSISTENT_HOME_MOUNT_POINT" > /root/umountPersistentHome.sh
    
    ln -s $PERSISTENT_HOME_MOUNT_POINT/PRIMAGE_HOME/$HOME_DIR_NAME /persistent-home
    
    if [ -n "$DATASET_REFERENCES_FILE" ]; then
        echo "======================="
        echo "Downloading last version of eform-downloader"
        curl -o /root/downloadEForms.py https://gitlab.com/primageproject/eform-downloader/-/raw/main/downloadEForms.py
        echo "Downloading Subject Eforms from $QUIBIM_HOST"
        python /root/downloadEForms.py --input $PERSISTENT_HOME_MOUNT_POINT/PRIMAGE_HOME/$HOME_DIR_NAME/$DATASET_REFERENCES_FILE --qp-host $QUIBIM_HOST \
                                       --user $QUIBIM_USER --password $QUIBIM_PASSWORD \
                                       --output-dir /dataset --qp-working-dir $MOUNT_POINT/PRIMAGE_DATALAKE
    fi
else
    echo "OneData access token not defined."
fi

echo "$@" >.runcmd
source .runcmd
