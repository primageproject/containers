
```
export IMAGE_NAME=primage/oneclient
export IMAGE_TAG=ub1804_one2002_cuda101
```

# Build the image tagging with the destination name and tag.
```
sudo docker build -t ${IMAGE_NAME}:${IMAGE_TAG} ./Docker
```

# Test deploying a container
```
sudo docker run -it --privileged --rm \
           -e ONECLIENT_ACCESS_TOKEN="yourToken" \
           -e ONECLIENT_PROVIDER_HOST="primagedatalakepublisher.datahub.egi.eu" \
           -e ONECLIENT_PROVIDER_HOST_FOR_HOME="upv.datahub.egi.eu" \
           -e MOUNT_POINT="/mnt/oneclient" \
           -e PERSISTENT_HOME_MOUNT_POINT="/mnt/oneclient-home" \
           -e HOME_DIR_NAME="yourPermanentHomeDirName" \
           -e DISABLE_SMB="true" \
           -e DATASET_REFERENCES_FILE="datasetReferences.json" \
           -e QUIBIM_HOST="primage.quibim.com" \
           -e QUIBIM_USER="yourUser" \
           -e QUIBIM_PASSWORD="yourPassword" \
           "${IMAGE_NAME}:${IMAGE_TAG}" \
           bash
```


# Upload the image to dockerHub
```
sudo docker login
sudo docker push ${IMAGE_NAME}:${IMAGE_TAG}
sudo docker logout
```

# Delete the local image
```
sudo docker rmi ${IMAGE_NAME}:${IMAGE_TAG}
```
