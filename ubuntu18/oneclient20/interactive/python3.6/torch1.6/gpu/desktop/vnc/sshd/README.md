
```
export IMAGE_NAME=primage/interactive
export IMAGE_TAG=ub1804_one2002_torch110_cuda101_desktop_vnc_sshd
```

# Build the image tagging with the destination name and tag.
```
sudo docker build -t ${IMAGE_NAME}:${IMAGE_TAG} ./Docker
```

# Test deploying a container
```
sudo docker run -d --privileged --rm -p 15900:5900 -p 3322:22 \
           -e ONECLIENT_ACCESS_TOKEN="yourToken" \
           -e ONECLIENT_PROVIDER_HOST="primagedatalakepublisher.datahub.egi.eu" \
           -e ONECLIENT_PROVIDER_HOST_FOR_HOME="upv.datahub.egi.eu" \
           -e MOUNT_POINT="/mnt/oneclient" \
           -e PERSISTENT_HOME_MOUNT_POINT="/mnt/oneclient-home" \
           -e HOME_DIR_NAME="yourPermanentHomeDirName" \
           -e DISABLE_SMB="true" \
           -e DATASET_REFERENCES_FILE="datasetReferences.json" \
           -e QUIBIM_HOST="primage.quibim.com" \
           -e QUIBIM_USER="yourUser" \
           -e QUIBIM_PASSWORD="yourPassword" \
           -e GUACAMOLE_HOST="upv.datahub.egi.eu" \
           -e GUACAMOLE_PORT="32443" \
           -e GUACAMOLE_PATH="guacamole" \
           -e GUACAMOLE_USER="yourUser" \
           -e GUACAMOLE_PASSWORD="yourPassword" \
           -e GUACD_HOST="guacamole-guacd.guacamole.svc.cluster.local" \
           -e USER="tensor" \
           -e PASSWORD="tensor" \
           -e SSH_ENABLE_PASSWORD_AUTH=true \
           -e GATEWAY_PORTS=true \
           -e TCP_FORWARDING=true \
           "${IMAGE_NAME}:${IMAGE_TAG}"
```

To access you must run a VNC client (for example tightVNC or tigerVNC) and connect to localhost:15900.
To transfer files connect with ssh client to localhost:3322

# Upload the image to dockerHub
```
sudo docker login
sudo docker push ${IMAGE_NAME}:${IMAGE_TAG}
sudo docker logout
```

# Delete the local image
```
sudo docker rmi ${IMAGE_NAME}:${IMAGE_TAG}
```
