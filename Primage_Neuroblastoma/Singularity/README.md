
#### IMPORTANT
The Singularity build will not work because the code directory is empty!

#### Neuroblastoma application
```bash
python train.py -i $INPUT_DATA_PATH -o $OUTPUT_DIRECTORY -e $EPOCH_NUMBER
python train_no_messages.py -i $INPUT_DATA_PATH -o $OUTPUT_DIRECTORY -e $EPOCH_NUMBER
```

#### Building Singularity container
Root privilegies. It is required to build in sandbox way due to version in Prometheus:
```bash
bash build.sh NAME.img
```
#### Execution
```bash
bash run.sh $INPUT_DATA_PATH $OUTPUT_DIRECTORY
```
#### Execution in Prometheus:
```bash
export SINGULARITY_LOCALCACHEDIR=$SCRATCH_LOCAL
dos2unix slurm_run.sh && sbatch -C localfs slurm_run.sh
```
#### Continuous integration
![Pipeline execution](docs/jenkins.JPG)


#### Singularity Registry

![Singularity container](docs/singularity_registry.JPG)

You can pull the image using `sregistry-cli` or using `singularity`:

##### Using sregistry-cli
If you have the credentials file of the sregistry in your home directory and the following alias in your .bashrc:
```bash
alias sregistry="docker run -it -v /tmp:/CONTAINERS -v $HOME/.sregistry:/CREDENTIALS -v $HOME/.singularity/:/root/.singularity -e SREGISTRY_CLIENT_SECRETS=/CREDENTIALS --network=host -e SREGISTRY_CLIENT=registry vanessa/sregistry-cli "
```
You must activate the registry:
```bash
sregistry backend activate registry
```  
Now, you can pull the image:
```bash
sregistry pull xenia/neuroblastoma:jenkins
```
##### Using singularity

Old example, the singularity registry is not available.
```bash
export SINGULARITY_IP=35.246.110.242 # Public IP
singularity pull --nohttps --library http://${SINGULARITY_IP} library://xenia/neuroblastoma:jenkins

```