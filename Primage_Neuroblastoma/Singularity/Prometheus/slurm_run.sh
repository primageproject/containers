#!/bin/bash -l

## Job name
#SBATCH -J neuroblastoma

## Number of allocated nodes
#SBATCH -N 1

## Number of tasks per node (by default this corresponds to the number of cores allocated per node) 
#SBATCH --ntasks-per-node=24

## Max task execution time (format is HH:MM:SS) 
#SBATCH --time=10:00:00

## Name of grant to which resource usage will be charged 
#SBATCH -A primage1

## Name of partition 
#SBATCH -p plgrid

## Name of file to which standard output will be redirected 
#SBATCH --output="slurm.out" 

## Name of file to which the standard error stream will be redirected 
#SBATCH --error="slurm.err"


## change to sbatch working directory 
cd $SLURM_SUBMIT_DIR 
srun /bin/hostname 

module load plgrid/tools/singularity
singularity -d run -B $SCRATCH/neuroblastoma/Training_Data:/training -B $SCRATCH/neuroblastoma/outputs:/output $HOME/neuroblastoma-sandbox.img python /app/train.py -i /training -o /output 
